from random import random
from kivy.uix.widget import Widget
from kivy.graphics import Color, Ellipse, Line


class StructureCanvas(Widget):
    def on_touch_down(self, touch):
        color = (random(), random(), random())
        with self.canvas:
            Color(*color)
            d = 5.0
            # Ellipse(pos=(touch.x - d / 2, touch.y - d / 2), size=(d, d))
            # touch.ud['line'] = Line(points=(touch.x, touch.y))
            Line(points=[touch.x, touch.y, touch.x, touch.y + 5, 100, 200], width=d)

    # def on_touch_move(self, touch):
    # touch.ud['line'].points += [touch.x, touch.y]
