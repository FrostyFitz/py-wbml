import json


class ProjectModel:
    def __init__(self):
        self.project_name = ""
        self.structure_elements = {}

    def save_json_project_file(self):
        pass

    def load_json_project_file(self, project_file):
        # Opening JSON file
        f = open(
            project_file,
        )
        data = json.load(f)

        print(data)
