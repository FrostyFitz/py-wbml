# System Imports
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.tabbedpanel import TabbedPanel
from os import listdir
from kivy.config import Config 
from kivy.core.window import Window
  
#Project Imports
import src.py.buttons
import src.py.menubar
import src.py.structurecanvas
from src.py.projectmodel import ProjectModel

Config.set('graphics', 'resizable', '0') 
Window.fullscreen = 'auto' 

#Load all UI Kivy definition files
kv_path = './src/UI/'
for kv in listdir(kv_path):
    Builder.load_file(kv_path+kv)

class MainTabPane(TabbedPanel):
    pass

class Container(FloatLayout):
    pass

class MainApp(App):

    def build(self):
        self.title = 'WhiteBoard Modeling Language'
        project = ProjectModel()
        project.load_json_project_file('./src/exampleproject.json')
        return Container()

if __name__ == "__main__":
    app = MainApp()
    app.run()